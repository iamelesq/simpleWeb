package render

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"path/filepath"

	"github.com/elesq/simpleweb/config"
	"github.com/elesq/simpleweb/pkg/models"
)

var app *config.AppConfig

// NewTemplates sets the config for the template package
func NewTemplates(a *config.AppConfig) {
	app = a
}

// create a map of funcions that can be used in the template
var functions = template.FuncMap{}

// AddDefaultData recieves the templateData and can be amended with data
// intended for every page of the site and returned.
func AddDefaultData(td *models.TemplateData) *models.TemplateData {
	return td
}

// RenderTemplate renders templates using html/template package in Go standard library.
// The func takes an http.ResponseWriter and a filename of the template to be rendered.
// Depending o the appConfig value of UseCache we use  the config Templatecache or we
// build a new templateCache. Pull the template from the cache, execute the template
// and pass the bytes written to the responseWriter.
func RenderTemplate(w http.ResponseWriter, tmpl string, td *models.TemplateData) {
	var tc map[string]*template.Template
	var err error

	if app.UseCache {
		tc = app.TemplateCache
	} else {
		tc, err = CreateTemplateCache()
		if err != nil {
			log.Fatal("could not create a TemplateCache object", err.Error())
		}
	}

	t, ok := tc[tmpl]
	if !ok {
		log.Fatal("could not get template from templateCache")
	}

	// create the buffer stream, pass the template data to
	// AddDefaultdata routine to add any default data and call
	// the execute the template with the buffer and the
	// template data.
	buf := new(bytes.Buffer)
	td = AddDefaultData(td)
	_ = t.Execute(buf, td)

	_, err = buf.WriteTo(w)
	if err != nil {
		log.Println("error writing template to the browser", err.Error())
	}

}

// CreateTemplateCache creates a template cache as a map
func CreateTemplateCache() (map[string]*template.Template, error) {
	myCache := map[string]*template.Template{}

	// creates a list of pages, files matching *.page.tmpl
	pages, err := filepath.Glob("./templates/*.page.tmpl")
	if err != nil {
		return myCache, err
	}

	for _, page := range pages {
		name := filepath.Base(page)

		//
		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
			return myCache, err
		}

		matches, err := filepath.Glob("./templates/*.layout.tmpl")
		if err != nil {
			return myCache, err
		}

		if len(matches) > 0 {
			_, err := ts.ParseGlob("./templates/*.layout.tmpl")
			if err != nil {
				return myCache, err
			}
		}

		myCache[name] = ts
	}

	return myCache, nil
}
