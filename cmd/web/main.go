package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/alexedwards/scs/v2"
	"github.com/elesq/simpleweb/config"
	"github.com/elesq/simpleweb/pkg/handlers"
	"github.com/elesq/simpleweb/pkg/render"
)

// create an app configuration
var app config.AppConfig

// session manager
var session *scs.SessionManager

// define the port we'll use to run
const PortNumber = ":8080"

func main() {
	// must be true for production mode
	app.InProduction = false

	// create a session with a 1 day lifetime
	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = app.InProduction

	// add the session to the appConfig
	app.Session = session

	// create a templatecache object
	tc, err := render.CreateTemplateCache()
	if err != nil {
		log.Fatal("cannot create a templateCache object")
	}

	// Assign the app's appConfig.TemplateCache
	// to the create templateCache object
	app.TemplateCache = tc
	app.UseCache = false

	// create a new respository that contains the appConfig
	// and pass it back to handlers that will use receivers
	// of the Repository type. This means a; handlers will
	// be able to access the appConfig
	repo := handlers.NewRepo(&app)
	handlers.NewHandlers(repo)

	render.NewTemplates(&app)

	fmt.Println("Listening on port", PortNumber)
	srv := &http.Server{
		Addr:    PortNumber,
		Handler: routes(&app),
	}

	err = srv.ListenAndServe()
	log.Fatal(err)

}
