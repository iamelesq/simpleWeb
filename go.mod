module github.com/elesq/simpleweb

replace github.com/elesq/simpleweb/handlers v0.0.0 => ./pkg/handlers

replace github.com/elesq/simpleweb/render v0.0.0 => ./pkg/render

go 1.16

require (
	github.com/alexedwards/scs/v2 v2.4.0 // indirect
	github.com/bmizerany/pat v0.0.0-20210406213842-e4b6760bdd6f
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/justinas/nosurf v1.1.1 // indirect
)
