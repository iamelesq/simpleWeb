# SimpleWeb

A simple templates based webapp example.

constructed using
- An AppConfig object to hold application state.
- A respository pattern to create the app repo that wraps a config, allowing state and config to be tapped into in a controlled manner.
- Templates based on the text/html templates available in the standard Library.
- Basic bootstrap inclusion for out-the-box style basics.
- 3rd party routers: pat & chi, pat for demo purposes and chi for middleware handling purposes
- 3rd party CSRF middleware package called noSurf
- 3rd party session handling scs